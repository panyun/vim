let $LANG='en_US'   "set message language
set langmenu=en     "set menu's language of gvim. no spaces beside '='


" Set vim-plug
call plug#begin('~/.vim/plugged')
Plug 'flazz/vim-colorschemes'
Plug 'felixhummel/setcolors.vim'
Plug 'tpope/vim-markdown'
Plug 'jtratner/vim-flavored-markdown'
Plug 'othree/html5.vim'
Plug 'pangloss/vim-javascript'
"Plug 'Valloric/YouCompleteMe'
Plug 'Shougo/neocomplcache.vim'
"Plug 'scrooloose/nerdcommenter'
Plug 'scrooloose/nerdtree'
Plug 'shime/vim-livedown'
Plug 'jiangmiao/auto-pairs'
Plug 'Shougo/unite.vim'
Plug 'mhinz/vim-startify'

call plug#end()

filetype plugin on
filetype plugin indent on

"set tabstop=8		" The width of a TAB is set to 8.
	        		" Still it is a \t. It is just that
			       	" Vim will interpret it to be having
        			" a width of 4.
"set shiftwidth=4	" Indents will have a width of 4
"set softtabstop=4	" Sets the number of columns for a TAB
"set expandtab		" Don't use actual tab character, expand TABs to spaces
"set smarttab		" Does the right thing (mostly) in programs
"set autoindent		" Turn on autoindent
set tabstop=4 softtabstop=0 expandtab shiftwidth=4 smarttab autoindent

" Use neocomplcache.
let g:neocomplcache_enable_at_startup = 1
" Use smartcase.
let g:neocomplcache_enable_smart_case = 1
" Set minimum syntax keyword length.
let g:neocomplcache_min_syntax_length = 3

" Set color scheme
colorscheme synic

" Set font
" set guifont=Source\ Code\ Pro:h11
set guifont=Consolas\ YaHei:h11

" Set line number
set relativenumber 
set number

" Set ruler
set ruler

" Set multi_byte support
set enc=utf-8
set fileencoding=utf-8
set fileencodings=ucs-bom,utf8,prc

" Map Ctrl+n to toggle NERDTree
map <C-n> :NERDTreeToggle<CR>

" Map <F12> to Markdown preview
nmap <F12> :LivedownToggle<CR>
